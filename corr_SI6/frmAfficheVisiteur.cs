﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace TP_gsb
{
    public partial class frmAfficheVisiteur : Form
    {
        MySqlDataReader ord = null;
        public frmAfficheVisiteur(MySqlDataReader sord)
        {
            InitializeComponent();
            
            ord = sord;
            ord.Read();
            tbid.Text = ord["id"].ToString();
            tbnom.Text = ord["nom"].ToString();
            tbprenom.Text = ord["prenom"].ToString();
            tbmdp.Text = ord["mdp"].ToString();
            tbadresse.Text = ord["adresse"].ToString();
            mtbcp.Text = ord["cp"].ToString();
            tbville.Text = ord["ville"].ToString();
            dtpEmbauche.Text = ord["dateEmbauche"].ToString();
        }

            
        /*
        private void reinitTb()
        {
            foreach (Control octrl in this.Controls)
            {
                if (octrl is TextBox)
                {
                    TextBox otb = (TextBox)octrl; //cast dans le bon type
                    otb.Text = string.Empty;
                }
                else if (octrl is MaskedTextBox) {
                    MaskedTextBox otb = (MaskedTextBox)octrl; //cast dans le bon type
                    otb.Text = string.Empty;

                }

            }


        }*/

        private void verifChkBox(object sender, EventArgs e)
        {
            if (chkboxmdp.Checked)
            {
                tbmdp.PasswordChar = '\0';
            }
            else
            {
                tbmdp.PasswordChar = '*';
            }
        }
    }
}
