﻿namespace TP_gsb
{
    partial class frmAfficheVisiteur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbnom = new System.Windows.Forms.TextBox();
            this.tbprenom = new System.Windows.Forms.TextBox();
            this.tbmdp = new System.Windows.Forms.TextBox();
            this.tbadresse = new System.Windows.Forms.TextBox();
            this.lbnom = new System.Windows.Forms.Label();
            this.lbprenom = new System.Windows.Forms.Label();
            this.lbmdp = new System.Windows.Forms.Label();
            this.lbadresse = new System.Windows.Forms.Label();
            this.lbcp = new System.Windows.Forms.Label();
            this.lbdateembauche = new System.Windows.Forms.Label();
            this.tbville = new System.Windows.Forms.TextBox();
            this.lbville = new System.Windows.Forms.Label();
            this.lbid = new System.Windows.Forms.Label();
            this.tbid = new System.Windows.Forms.TextBox();
            this.mtbcp = new System.Windows.Forms.MaskedTextBox();
            this.chkboxmdp = new System.Windows.Forms.CheckBox();
            this.dtpEmbauche = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // tbnom
            // 
            this.tbnom.Location = new System.Drawing.Point(119, 84);
            this.tbnom.Margin = new System.Windows.Forms.Padding(2);
            this.tbnom.Name = "tbnom";
            this.tbnom.Size = new System.Drawing.Size(130, 20);
            this.tbnom.TabIndex = 1;
            // 
            // tbprenom
            // 
            this.tbprenom.Location = new System.Drawing.Point(119, 128);
            this.tbprenom.Margin = new System.Windows.Forms.Padding(2);
            this.tbprenom.Name = "tbprenom";
            this.tbprenom.Size = new System.Drawing.Size(130, 20);
            this.tbprenom.TabIndex = 2;
            // 
            // tbmdp
            // 
            this.tbmdp.Location = new System.Drawing.Point(119, 165);
            this.tbmdp.Margin = new System.Windows.Forms.Padding(2);
            this.tbmdp.Name = "tbmdp";
            this.tbmdp.PasswordChar = '*';
            this.tbmdp.Size = new System.Drawing.Size(128, 20);
            this.tbmdp.TabIndex = 3;
            // 
            // tbadresse
            // 
            this.tbadresse.Location = new System.Drawing.Point(119, 217);
            this.tbadresse.Margin = new System.Windows.Forms.Padding(2);
            this.tbadresse.Multiline = true;
            this.tbadresse.Name = "tbadresse";
            this.tbadresse.Size = new System.Drawing.Size(158, 44);
            this.tbadresse.TabIndex = 5;
            // 
            // lbnom
            // 
            this.lbnom.AutoSize = true;
            this.lbnom.Location = new System.Drawing.Point(18, 84);
            this.lbnom.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbnom.Name = "lbnom";
            this.lbnom.Size = new System.Drawing.Size(35, 13);
            this.lbnom.TabIndex = 6;
            this.lbnom.Text = "Nom :";
            // 
            // lbprenom
            // 
            this.lbprenom.AutoSize = true;
            this.lbprenom.Location = new System.Drawing.Point(18, 128);
            this.lbprenom.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbprenom.Name = "lbprenom";
            this.lbprenom.Size = new System.Drawing.Size(49, 13);
            this.lbprenom.TabIndex = 7;
            this.lbprenom.Text = "Prénom :";
            // 
            // lbmdp
            // 
            this.lbmdp.AutoSize = true;
            this.lbmdp.Location = new System.Drawing.Point(20, 167);
            this.lbmdp.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbmdp.Name = "lbmdp";
            this.lbmdp.Size = new System.Drawing.Size(77, 13);
            this.lbmdp.TabIndex = 8;
            this.lbmdp.Text = "Mot de passe :";
            // 
            // lbadresse
            // 
            this.lbadresse.AutoSize = true;
            this.lbadresse.Location = new System.Drawing.Point(18, 219);
            this.lbadresse.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbadresse.Name = "lbadresse";
            this.lbadresse.Size = new System.Drawing.Size(51, 13);
            this.lbadresse.TabIndex = 9;
            this.lbadresse.Text = "Adresse :";
            // 
            // lbcp
            // 
            this.lbcp.AutoSize = true;
            this.lbcp.Location = new System.Drawing.Point(18, 281);
            this.lbcp.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbcp.Name = "lbcp";
            this.lbcp.Size = new System.Drawing.Size(69, 13);
            this.lbcp.TabIndex = 10;
            this.lbcp.Text = "Code postal :";
            // 
            // lbdateembauche
            // 
            this.lbdateembauche.AutoSize = true;
            this.lbdateembauche.Location = new System.Drawing.Point(18, 357);
            this.lbdateembauche.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbdateembauche.Name = "lbdateembauche";
            this.lbdateembauche.Size = new System.Drawing.Size(89, 13);
            this.lbdateembauche.TabIndex = 11;
            this.lbdateembauche.Text = "Date embauche :";
            // 
            // tbville
            // 
            this.tbville.Location = new System.Drawing.Point(117, 315);
            this.tbville.Margin = new System.Windows.Forms.Padding(2);
            this.tbville.Name = "tbville";
            this.tbville.Size = new System.Drawing.Size(128, 20);
            this.tbville.TabIndex = 7;
            // 
            // lbville
            // 
            this.lbville.AutoSize = true;
            this.lbville.Location = new System.Drawing.Point(18, 318);
            this.lbville.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbville.Name = "lbville";
            this.lbville.Size = new System.Drawing.Size(32, 13);
            this.lbville.TabIndex = 14;
            this.lbville.Text = "Ville :";
            // 
            // lbid
            // 
            this.lbid.AutoSize = true;
            this.lbid.Location = new System.Drawing.Point(18, 37);
            this.lbid.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbid.Name = "lbid";
            this.lbid.Size = new System.Drawing.Size(62, 13);
            this.lbid.TabIndex = 16;
            this.lbid.Text = "Identidiant :";
            // 
            // tbid
            // 
            this.tbid.Location = new System.Drawing.Point(117, 35);
            this.tbid.Margin = new System.Windows.Forms.Padding(2);
            this.tbid.Name = "tbid";
            this.tbid.Size = new System.Drawing.Size(130, 20);
            this.tbid.TabIndex = 0;
            // 
            // mtbcp
            // 
            this.mtbcp.Location = new System.Drawing.Point(117, 279);
            this.mtbcp.Margin = new System.Windows.Forms.Padding(2);
            this.mtbcp.Mask = "00000";
            this.mtbcp.Name = "mtbcp";
            this.mtbcp.Size = new System.Drawing.Size(52, 20);
            this.mtbcp.TabIndex = 6;
            // 
            // chkboxmdp
            // 
            this.chkboxmdp.AutoSize = true;
            this.chkboxmdp.Location = new System.Drawing.Point(131, 188);
            this.chkboxmdp.Margin = new System.Windows.Forms.Padding(2);
            this.chkboxmdp.Name = "chkboxmdp";
            this.chkboxmdp.Size = new System.Drawing.Size(109, 17);
            this.chkboxmdp.TabIndex = 4;
            this.chkboxmdp.Text = "voir mot de passe";
            this.chkboxmdp.UseVisualStyleBackColor = true;
            this.chkboxmdp.Click += new System.EventHandler(this.verifChkBox);
            // 
            // dtpEmbauche
            // 
            this.dtpEmbauche.Location = new System.Drawing.Point(117, 353);
            this.dtpEmbauche.Margin = new System.Windows.Forms.Padding(2);
            this.dtpEmbauche.Name = "dtpEmbauche";
            this.dtpEmbauche.Size = new System.Drawing.Size(172, 20);
            this.dtpEmbauche.TabIndex = 17;
            // 
            // frmAfficheVisiteur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(310, 433);
            this.Controls.Add(this.dtpEmbauche);
            this.Controls.Add(this.chkboxmdp);
            this.Controls.Add(this.mtbcp);
            this.Controls.Add(this.tbid);
            this.Controls.Add(this.lbid);
            this.Controls.Add(this.lbville);
            this.Controls.Add(this.tbville);
            this.Controls.Add(this.lbdateembauche);
            this.Controls.Add(this.lbcp);
            this.Controls.Add(this.lbadresse);
            this.Controls.Add(this.lbmdp);
            this.Controls.Add(this.lbprenom);
            this.Controls.Add(this.lbnom);
            this.Controls.Add(this.tbadresse);
            this.Controls.Add(this.tbmdp);
            this.Controls.Add(this.tbprenom);
            this.Controls.Add(this.tbnom);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximumSize = new System.Drawing.Size(327, 479);
            this.Name = "frmAfficheVisiteur";
            this.Text = "Détails visiteur commercial";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbnom;
        private System.Windows.Forms.TextBox tbprenom;
        private System.Windows.Forms.TextBox tbmdp;
        private System.Windows.Forms.TextBox tbadresse;
        private System.Windows.Forms.Label lbnom;
        private System.Windows.Forms.Label lbprenom;
        private System.Windows.Forms.Label lbmdp;
        private System.Windows.Forms.Label lbadresse;
        private System.Windows.Forms.Label lbcp;
        private System.Windows.Forms.Label lbdateembauche;
        private System.Windows.Forms.TextBox tbville;
        private System.Windows.Forms.Label lbville;
        private System.Windows.Forms.Label lbid;
        private System.Windows.Forms.TextBox tbid;
        private System.Windows.Forms.MaskedTextBox mtbcp;
        private System.Windows.Forms.CheckBox chkboxmdp;
        private System.Windows.Forms.DateTimePicker dtpEmbauche;
    }
}