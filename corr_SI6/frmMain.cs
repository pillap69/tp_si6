﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TP_gsb.mesClasses.outils;
using TP_gsb;

namespace corr_SI6
{
    public partial class frmMain : Form
    {
        Button btnRecherche = null;
        TextBox tbRecherche = null;
        public frmMain()
        {
           
            InitializeComponent();
            tbAffiche.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            

        }

        private void creerBtnRecherche()
        {
            
            //Création et paramétrage du bouton RECHERCHE

            btnRecherche = new Button();
            this.Controls.Add(this.btnRecherche); //ajout à la collection de contrôles de la feuille
            this.btnRecherche.Location = new System.Drawing.Point(271, 272);
            this.btnRecherche.Name = "btnRecherche";
            this.btnRecherche.Size = new System.Drawing.Size(140, 55);
            this.btnRecherche.TabIndex = 1;
            this.btnRecherche.Text = "Rechercher";
            this.btnRecherche.UseVisualStyleBackColor = true;
            this.btnRecherche.Click += new EventHandler(afficheDetailsVisiteur);


        }

        private void afficheDetailsVisiteur(object sender, EventArgs e)
        {
            try
            {
                string[] tabPrenomNom = null;
                tabPrenomNom = tbRecherche.Text.Trim().Split(' ');
                string query = "select * from visiteur where prenom='" + tabPrenomNom[0] + "'" + " and nom='" + tabPrenomNom[1] + "'";
                Cdao odao = new Cdao();
                MySqlDataReader ord = odao.getReader(query);
                frmAfficheVisiteur ofrmAV = new frmAfficheVisiteur(ord);
                ofrmAV.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }



        private void creerTbRecherche()
        {
            this.tbRecherche = new TextBox();
            this.Controls.Add(this.tbRecherche);
            this.tbRecherche.Location = new System.Drawing.Point(51, 272);
            this.tbRecherche.Multiline = false;
            this.tbRecherche.Name = "tbRecherche";
            this.tbRecherche.Size = new System.Drawing.Size(360 - (this.btnRecherche.Width + 10), 188);
            this.tbRecherche.TabIndex = 0;

        }

        private void afficheListeDesVisiteursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tbAffiche.Clear();
            
            string requete = "select id,prenom,nom from visiteur";
            Cdao odao = new Cdao();
            MySqlDataReader ord = odao.getReader(requete);
            while (ord.Read())
            {
                //tbAffiche.Text = tbAffiche.Text + ord[0] + "  " + ord[1] + "  " + ord[2] + "\r\n";
                tbAffiche.Text = tbAffiche.Text + ord["id"] + "  " + ord["prenom"] + "  " + ord["nom"] + "\r\n";

            }
            ord.Close();
            creerBtnRecherche();
            creerTbRecherche();

        }
    }
}
