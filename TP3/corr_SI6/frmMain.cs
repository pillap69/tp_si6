﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TP_gsb.mesClasses.outils; // namespace de la classe outil

namespace corr_SI6
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
           
            InitializeComponent();
            tbAffiche.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;


        }

        

        private void afficherListeVisiteursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string query = "select id,nom,prenom from visiteur";
            //instanciation de la classe outil Cdao
            Cdao odao = new Cdao();
            //Appel de la méthode fonction getReader qui retourne un jeu d'enregistrement
            MySqlDataReader ord = odao.getReader(query);
            //Boucle qui itère sur le jeu d'enregistrement 
            while (ord.Read()) //tant qu'il y a des données à lire
            {
                // Construction d'une chaine à partir de la classe stringBuilder
                StringBuilder osb = new StringBuilder();
                osb.Append(Convert.ToString(ord["id"]) + "  " + Convert.ToString(ord["prenom"]) + "  " +
                Convert.ToString(ord["nom"]));
                // Affichage des données dans la textBox
                tbAffiche.Text = tbAffiche.Text + osb.ToString() + "\r\n"; 
            }
            ord.Close(); //fermeture du reader
        }
    }
}
